#!/usr/bin/env python

import argparse
import PyPDF2 as pypdf
import sys
import os
import re
import datetime
from time import strptime

RE_SEMESTER = "(fall|spring|summer)\s+(\d{4})"
RE_DATE = "(jan(?:uary)?|feb(?:ruary)?|mar(?:ch)?|apr(?:il)?|may|jun(?:e)?|jul(?:y)?|aug(?:ust)?|sep(?:tember)?|oct(?:ober)?|nov(?:ember)?|dec(?:ember)?)\s+(\d{1,2})"
MD_DATE = "%m-%d-%Y (%a)"

class MDCal:
    def __init__(self, pdf_file):
        self.pdf = pdf_file
        self.__parse()

    def __parse(self):
        reader = pypdf.PdfReader(self.pdf)
        #page = reader.getPage(0)
        page = reader.pages[0]
        #self.__pdftxt = page.extractText().replace(os.linesep, "").lower()
        self.__pdftxt = page.extract_text().replace(os.linesep, "").lower()
        self.term, self.year = self.__find_first_semester()
        self.start = self.__find_first_date_after("classes begin")
        final_end = self.__find_first_date_after("final grading period")
        # Quick trick to calculate the last date
        self.end = final_end + datetime.timedelta(days=13)

    def __find_first_semester(self):
        pdftxt = self.__pdftxt
        m = re.search(RE_SEMESTER, pdftxt)
        if m == None:
            return None
        semester = m.groups()
        self.__pdftxt = pdftxt[m.end():]
        return semester[0].capitalize(), int(semester[1])

    def __find_first_date_after(self, exp):
        splits = self.__pdftxt.split(exp, 1)
        if len(splits) == 0:
            return None
        pdftxt = splits[1]
        m = re.search(RE_DATE, pdftxt)
        if m == None:
            return None
        date = m.groups()
        self.__pdftxt = pdftxt[m.end():]
        return datetime.datetime(self.year, strptime(date[0],"%B").tm_mon, \
                        int(date[1]))

    def write(self, md_file):
        f = md_file
        f.write("<!-- Author: Chung Hwan Kim -->" + os.linesep)
        f.write(os.linesep)
        f.write(("%s %d To-Do Items" + os.linesep) % (self.term, self.year))
        f.write("===" + os.linesep)
        f.write(os.linesep)
        for x in range((self.end - self.start).days + 1):
            d = self.start + datetime.timedelta(days=x)
            f.write(("## %s" + os.linesep) % (d.strftime(MD_DATE)))
            f.write(os.linesep)
            # Sunday?
            if d.weekday() == 6 and d != self.end:
                f.write("---" + os.linesep)
                f.write(os.linesep)
        f.write("<!-- EOF -->" + os.linesep)
        f.close()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("InputPDF",
                        help="Name of input PDF calendar file",
                        type=argparse.FileType("rb"))
    parser.add_argument("OutputMD",
                        help="Name of output MD file",
                        type=argparse.FileType("w"))
    args = parser.parse_args()

    sys.stderr.write("Loading '%s' ... " % \
                     (os.path.basename(args.InputPDF.name)))

    mdcal = MDCal(args.InputPDF)

    sys.stderr.write("Complete" + os.linesep)

    print("* Semester:    %s %d" % (mdcal.term, mdcal.year))
    print("* Start date:  %s" % (mdcal.start.strftime(MD_DATE)))
    print("* End date:    %s" % (mdcal.end.strftime(MD_DATE)))

    sys.stderr.write("Writing '%s' ... " % \
                     (os.path.basename(args.OutputMD.name)))

    mdcal.write(args.OutputMD)

    sys.stderr.write("Complete" + os.linesep)

if __name__ == "__main__":
    main()
